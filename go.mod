module github.com/johnsonjh/gpushover

go 1.17

require (
	github.com/johnsonjh/leaktestfe v0.0.0-20221210113806-1ad56057a826
	github.com/json-iterator/go v1.1.13-0.20220915233716-71ac16282d12
	go4.org v0.0.0-20201209231011-d4a079459e60
)

require (
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	go.uber.org/goleak v1.2.0 // indirect
	golang.org/x/tools v0.4.1-0.20221213194812-9ec855317b92 // indirect
)
